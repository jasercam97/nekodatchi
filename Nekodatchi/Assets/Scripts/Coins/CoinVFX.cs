﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using System;
using UnityEngine.SceneManagement;

public class CoinVFX : Singleton<CoinVFX>
{
    public GameObject animatedCoinPrefab;

    [Range(0.5f, 1.5f)]
    public float animationTimeMin;
    [Range(1f, 2.5f)]
    public float animationTimeMax;
    public Ease EaseType;
    public float spread = 2f;

    public AudioClip CoinClip;


    public Transform TargetPos;

    void Start()
    {
        //_targetPos = Camera.main.ScreenToWorldPoint(CanvasManager.Instance.GetCoinHUDPos().position);
    }

    private void PrepareCoins()
    {
        GameObject coin;
        coin = Instantiate(animatedCoinPrefab, transform);

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="collectedCoinPos"> La posicion de la moneda cogida (Para hacer la animación)</param>
    /// <param name="ammount"></param>
    public void AddCoins(Vector3 collectedCoinPos, int ammount)
    {
        Animate(collectedCoinPos, ammount);
    }

    private void Animate(Vector3 collectedCoinPos, int ammount)
    {
        for(int i = 0; i < ammount; i++)
        {
            GameObject coin;

            coin = Instantiate(animatedCoinPrefab, transform);

            coin.transform.position = collectedCoinPos + new Vector3(UnityEngine.Random.Range(-spread, spread), 0f, 0f);

            coin.transform.DOMove(TargetPos.position, UnityEngine.Random.Range(animationTimeMin, animationTimeMax))
                .SetEase(EaseType)
                .OnComplete(() =>
                {
                    coin.SetActive(false);
                    AddCoinComplete(1);
                    AudioManager.Instance.PlayOneClipSFX(CoinClip);
                })
                .Play();


        }
    }

    private void AddCoinComplete(int ammount)
    {
        GameManager.Instance.Money += ammount;
    }
}
