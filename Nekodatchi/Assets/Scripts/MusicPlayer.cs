﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public AudioClip Music;
    // Start is called before the first frame update
    void Start()
    {
        AudioManager.Instance.PlayMusic(Music);
    }
}
