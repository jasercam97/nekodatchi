﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoopBehavior : MonoBehaviour
{
    [Header ("Basic Properties")]
    public int MinLife = 10;
    public int MaxLife = 20;
    public ParticleSystem PoopPS;

    [Header("Golden Poop")]
    public bool GoldenPoop;
    public int MinCoins = 1;
    public int MaxCoins = 3;

    [Header("Sound")]
    public AudioClip Caca;
    public AudioClip CleanCaca;

    private int _life;

    void Start()
    {
        _life = Random.Range(MinLife, MaxLife);
        AudioManager.Instance.PlayOneClipSFX(Caca);
    }

    private void OnMouseEnter()
    {
        _life--;
        PoopPS.Play();
        if (_life == 0)
            DestroyPoop();
    }

    private void DestroyPoop()
    {
        GameManager.Instance.CleanPoop();
        AudioManager.Instance.PlayOneClipSFX(CleanCaca);

        Destroy(gameObject);
        if (GoldenPoop)
            CoinVFX.Instance.AddCoins(transform.position, Random.Range(MinCoins, MaxCoins));
    }
}
