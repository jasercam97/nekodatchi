﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooper : MonoBehaviour
{

    public float MinTimePoop = 5f;
    public float MaxTimePoop = 15f;

    public float MinXSpawn = -5f;
    public float MaxXSpawn = 5f;

    public float MinYSpawn = -2f;
    public float MaxYSpawn = 3f;

    public GameObject PoopObj;
    public GameObject GoldenPoopObj;

    public Transform _spawnPos;
    void Start()
    {
        Invoke(nameof(Poop), Random.Range(MinTimePoop, MaxTimePoop));
    }

    private void Poop()
    {
        _spawnPos.position = new Vector3(Random.Range(MinXSpawn, MaxXSpawn), transform.position.y + Random.Range(MinYSpawn, MaxYSpawn), transform.position.z);
        if (Random.value < 0.7f)
            Instantiate(PoopObj, _spawnPos.position, PoopObj.transform.rotation);
        else
            Instantiate(GoldenPoopObj, _spawnPos.position, GoldenPoopObj.transform.rotation);
        Invoke(nameof(Poop), Random.Range(MinTimePoop, MaxTimePoop));
    }
}
