﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveJumping : MonoBehaviour
{

    public float WaitingBetweenMoves;

    public float jumpPower;

    public int jumpsBetweenMoves;

    public float TimeMoving;

    public List<Transform> wayPoints;

    private Coroutine jump;

    void Start()
    {
        jump = StartCoroutine(jumpToDestiny());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator jumpToDestiny()
    {

        while(true)
        {
            yield return new WaitForSeconds(WaitingBetweenMoves);
            transform.DOLocalJump(wayPoints[Random.Range(0, wayPoints.Count)].position, jumpPower, jumpsBetweenMoves, TimeMoving).SetEase(Ease.Linear).Play();
        }

    }

    private void OnDestroy()
    {
        StopCoroutine(jump);
    }
}
