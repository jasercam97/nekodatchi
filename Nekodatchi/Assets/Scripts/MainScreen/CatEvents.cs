﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatEvents : MonoBehaviour
{

    public Animator anim;

    bool _mouseIsMoving;

    Vector2 _mouseLastPosition;

    private void OnMouseOver()
    {
        Vector3 ScreenPoint = Input.mousePosition;
        ScreenPoint.z = Camera.main.transform.position.z;
        ScreenPoint = new Vector3(Camera.main.ScreenToWorldPoint(ScreenPoint).x, transform.position.y, transform.position.z);

        if (!AnimatorIsPlaying() && Input.GetMouseButton(0) && (Vector2) ScreenPoint != _mouseLastPosition)
        {
            anim.gameObject.SetActive(true);
            GameManager.Instance.TimesPet++;
        }

        
        _mouseLastPosition = ScreenPoint;
    }

    void DeactivateHearts()
    {
        anim.gameObject.SetActive(false);
    }

    bool AnimatorIsPlaying()
    {
        if(anim.gameObject.activeInHierarchy)
            return anim.GetCurrentAnimatorStateInfo(0).length > anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
        else 
            return false;
    }
}
