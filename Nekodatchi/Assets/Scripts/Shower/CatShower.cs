﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatShower : MonoBehaviour
{
    public float speedMin = 2f;
    public float speedMax = 5f;

    public float YMax = 3.5f;
    public float YMin = -3.5f;

    private float XMax = 6.5f;
    private float XMin = -6.5f;
    [SerializeField]
    private float speed;
    private int _dir = 1;
    private void Start()
    {
        RandomSpeedDir();
    }

    private void FlipCat()
    {
        _dir *= -1;
        transform.Rotate(new Vector3(0f, 180f, 0f));
        RandomSpeedDir();
    }
    private void RandomSpeedDir()
    {
        speed = _dir * Random.Range(speedMin, speedMax);
        transform.position = new Vector3(transform.position.x, Random.Range(YMin, YMax), transform.position.z);
    }

    void Update()
    {
        transform.Translate(transform.right * speed * Time.deltaTime);

        if ((transform.position.x < XMin && _dir == -1)|| (transform.position.x > XMax && _dir ==1))
            FlipCat();
    }
}
