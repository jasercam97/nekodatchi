﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawnerShower : MonoBehaviour
{
    private float XMax = 6.5f;
    private float XMin = -6.5f;

    public float YMax = 3.5f;
    public float YMin = -3.5f;

    public float MinSpawn = 1f;
    public float MaxSpawn = 5f;

    public GameObject CoinShower;
    private Vector3 SpawnPos;
    private bool PlayingGame = false;
    void Start()
    {
        Invoke(nameof(SpawnCoin), Random.Range(MinSpawn, MaxSpawn));
    }

    private void SpawnCoin()
    {
        if(PlayingGame)
        {
            SpawnPos = new Vector3(0f, Random.Range(YMin, YMax), 0f);
            if (Random.value <= 0.5f)
                SpawnPos.x += XMax;
            else
                SpawnPos.x += XMin;

            Instantiate(CoinShower, SpawnPos, CoinShower.transform.rotation);
        }

        Invoke(nameof(SpawnCoin), Random.Range(MinSpawn, MaxSpawn));
    }

    public void StartPlaying()
    {
        PlayingGame = true;
    }

    public void StopPlaying()
    {
        PlayingGame = false;
    }
}
