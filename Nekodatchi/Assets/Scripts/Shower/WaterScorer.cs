﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterScorer : MonoBehaviour
{
    public ShowerGame ShowerGame;

    public int PointsPerTic = 1;

    public float TimeLoseWater = 1f;
    public int WaterLoss = 1;

    public AudioClip Water;
    private void OnEnable()
    {
        InvokeRepeating(nameof(LoseWater), 0f, TimeLoseWater);
        AudioManager.Instance.PlayLoopSFX(Water);

    }
    private void OnDisable()
    {
        CancelInvoke(nameof(LoseWater));
        AudioManager.Instance.StopLoopSFX(Water);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
            ShowerGame.AddPoints(PointsPerTic);
        if (collision.CompareTag("Coin"))
            collision.GetComponent<CoinShowerBehavior>().LoseLife();

    }

    private void LoseWater()
    {
        ShowerGame.LoseWater(WaterLoss);
    }
}
