﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShowerGame : MonoBehaviour
{
    [Header("Properties")]
    public float Duration = 60f;
    public int MaxWater = 100;

    [Header("HUD")]
    public GameObject End_Hud;
    public TutorialPanel tutPanel;
    public FadePanel fade;

    public TextMeshProUGUI scoreText;
    public string ScoreStringInitial = "Score: \n";

    public Slider WaterSlider;

    [Header("Objects To Deactivate on End")]
    public GameObject Alcachofa;
    public GameObject Gatete;

    [Header("SceneManager")]
    public int MainSceneIndex = 1;
    public CoinSpawnerShower CSS;


    private int score = 0;
    private int WaterResource;

    private void Awake()
    {
        GameManager.Instance.StartMinigame();
    }

    void Start()
    {
        fade.FadeOut().onComplete = () => { tutPanel.OpenPanel(); };
        Alcachofa.SetActive(false);
        Gatete.SetActive(false);
    }

    public void StartGame()
    {
        End_Hud.SetActive(false);
        Invoke(nameof(EndGame), Duration);

        AddPoints(0);

        WaterResource = MaxWater;
        LoseWater(0);

        Alcachofa.SetActive(true);
        Gatete.SetActive(true);

        CSS.StartPlaying();
    }

    public void AddPoints(int points)
    {
        score += points;

        scoreText.text = ScoreStringInitial + "\n" + score.ToString();
    }

    public void LoseWater(int ammount)
    {
        WaterResource -= ammount;

        WaterSlider.value = (float) WaterResource / MaxWater;

        if (WaterResource <= 0)
            EndGame();

    }
    private void EndGame()
    {
        CancelInvoke(nameof(EndGame));

        End_Hud.GetComponentInChildren<TextMeshProUGUI>().text = "Final Score: \n" + score.ToString();
        End_Hud.SetActive(true);

        scoreText.gameObject.SetActive(false);

        Alcachofa.SetActive(false);
        Gatete.SetActive(false);

        CSS.StopPlaying();

        GameManager.Instance.FinishHygieneGame(score);
    }

    public void ReturnToMainScene()
    {
        SceneManager.LoadScene(MainSceneIndex);
    }
}
