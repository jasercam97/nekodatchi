﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowWater : MonoBehaviour
{
    public GameObject Water;
    private void Start()
    {
        Water.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            Water.SetActive(true);
        if (Input.GetMouseButtonUp(0))
            Water.SetActive(false);
    }
}
