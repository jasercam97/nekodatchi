﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinShowerBehavior : MonoBehaviour
{
    public int life = 15;

    public float MinSpeed = 5f;
    public float MaxSpeed = 10f;

    private float XMax = 6.5f;
    private float XMin = -6.5f;

    private float _speed;
    void Start()
    {
        RandomSpeed();
    }

    private void RandomSpeed()
    {
        _speed = Random.Range(MinSpeed, MaxSpeed);
        if (transform.position.x > 0)
            _speed *= -1;
    }

    void Update()
    {
        transform.Translate(transform.right * _speed * Time.deltaTime);

        if ((_speed < 0 && transform.position.x < XMin) || (_speed > 0 && transform.position.x > XMax))
            Destroy(gameObject);
    }

    public void LoseLife()
    {
        life--;
        if(life <= 0)
        {
            GameManager.Instance.MoneyToAdd++;
            Destroy(gameObject);
        }
    }
}
