﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GlassCoins : MonoBehaviour
{
    private int coinsInGlass = 0; // Monedas dentro del vaso
    public Vector2 randomCoinsInGlass; //Min o max de monedas que puede tener un vaso
    public GameObject _coinCollector; //objeto encargado de almacenar las monedas
    public GameObject _glassManager; // Objeto encargado de ir haciendo aparecer los vasos

    public AudioClip GlassBroken;

    private void OnEnable()
    {
        _coinCollector = GameObject.Find("CoinCollector"); //Asignamos el manager de contar las monedas
        _glassManager = GameObject.Find("GlassManager"); //Asignamos el manager de instanciar los vasos
        coinsInGlass = Random.Range((int) randomCoinsInGlass.x, (int) randomCoinsInGlass.y + 1); //Setea las monedas que tenga el vaso
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Floor"))
        {
            _coinCollector.GetComponent<CoinCollector>().AddCoins(coinsInGlass); //Añade las monedas a la score de monedas
            Invoke(nameof(NewGlass), 0.5f); //Instancia el nuevo vaso
            Invoke(nameof(DestroyGlass), 1f); //Destuye el vaso actual
        }
    }    

    private void DestroyGlass() //Destuye el vaso actual
    {
        AudioManager.Instance.PlayOneClipSFX(GlassBroken);
        Destroy(gameObject);
    }

    private void NewGlass() //Instancia el nuevo vaso con el GlassManager
    {
        _glassManager.GetComponent<GlassManager>().InstantiateGlass();
    }
}
