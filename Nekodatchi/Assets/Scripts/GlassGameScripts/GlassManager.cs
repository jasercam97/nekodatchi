﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassManager : MonoBehaviour
{
    public GameObject glassPrefab;
    public Transform glassSpawnPosition;
    private GameObject _glass; //Placeholder para el vaso que se instancia

    public void InstantiateGlass() //Método para instanciar vasos
    {
        _glass = glassPrefab;
        Instantiate(_glass, glassSpawnPosition.position, _glass.transform.rotation);
    }

    private void Start()
    {
        InstantiateGlass();
    }

}
