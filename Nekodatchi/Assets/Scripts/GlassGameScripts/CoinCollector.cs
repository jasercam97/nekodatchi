﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinCollector : MonoBehaviour
{
    public int CoinsCollected = 0; //Monedas conseguidas

    public TextMeshProUGUI coinsText; //Referencia al contexto del canvas

    public void AddCoins(int amount)
    {
        CoinsCollected += amount; //Añade monedas a la score final
        UpdateCoins(); //Las actualiza en la UI
    }

    public void UpdateCoins()
    {
        coinsText.text = "SCORE \n"  + CoinsCollected.ToString();
    }

    private void OnEnable()
    {
        UpdateCoins();
    }
}
