﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HumanBehaviour : MonoBehaviour
{
    [Header("Human check")]
    public Vector2 lookingCatTime;//Tiempo min y max que se queda mirando al gato
    public Vector2 noLookingCatTime;//Tiempo min y max entre mirada y mirada al gato

    private bool isLookingAtCat = false; // Controla si esta mirando o no al gato
    private float auxTimer; //Timer auxiliar

    public GameObject animationObject;
    private GameObject _glass;
    private GameObject _glassManager;

    private void Start()
    {
        auxTimer = Random.Range(noLookingCatTime.x, noLookingCatTime.y);
        gameObject.GetComponent<SpriteRenderer>().flipX = true;
        _glassManager = GameObject.Find("GlassManager");
    }

    private void Update()
    {
        SearchTheGlass();
        WaitForLooking();
        if (isLookingAtCat)
        {
            if (_glass.GetComponent<GlassMovement>().isBeingPushed)
            {
                Destroy(_glass);
                _glassManager.GetComponent<GlassManager>().InstantiateGlass();
            }
        }
    }

    private void SearchTheGlass()
    {
        _glass = GameObject.Find("Vaso(Clone)");
    }

    private void WaitForLooking()
    {
        if (!isLookingAtCat) //Si no estoy mirando al gato avanza el timer de no mirarlo
        {
            auxTimer -= Time.deltaTime;
            if(auxTimer <= 0.5f)
            {
                animationObject.SetActive(true);
            }

            if (auxTimer <= 0)
            {
                animationObject.SetActive(false);
                auxTimer = Random.Range(lookingCatTime.x, lookingCatTime.y); //Reiniciamos el timer con un random
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
                isLookingAtCat = true;
            }
        }
        else // Aqui avanza el timer de cuando lo esta mirando
        {
            auxTimer -= Time.deltaTime;
            if (auxTimer <= 0)
            {
                isLookingAtCat = false;
                auxTimer = Random.Range(noLookingCatTime.x, noLookingCatTime.y); //Reiniciamos el timer con un random
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
        }
    }
}
