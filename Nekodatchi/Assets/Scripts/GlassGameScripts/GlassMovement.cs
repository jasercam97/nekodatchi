﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GlassMovement : MonoBehaviour
{
    [Header("Glass movement speed")]
    public float speedMovement; //Velocidad de movimiento del vaso
    public bool isBeingPushed = false; //Booleano para comprobar si se esta clickando el vaso

    private void Update()
    {
        if (isBeingPushed) //Si no lo está mirando y clickamos el vaso se mueve
        {
            transform.Translate(Vector2.left * speedMovement * Time.deltaTime);
        }
    }

    private void OnMouseDown()
    {
        if (!TimeManager.Instance.GameFinished)
        {
            isBeingPushed = true; // Si clickamos sobre el vaso
        }
    }

    private void OnMouseUp()
    {
        isBeingPushed = false; // Si dejamos de clickar sobre el vaso
    }
}
