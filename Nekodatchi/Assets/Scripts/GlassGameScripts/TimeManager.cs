﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TimeManager : Singleton<TimeManager>
{
    public float timeToPlay; //Tiempo que dura el minijuego
    public GameObject finishPanel; //Panel par activar cuando acabe
    public TextMeshProUGUI coinsText; //Texto de la final score
    public Slider timeSlider; //Slider del tiempo

    public TutorialPanel tutPanel;

    public bool GameFinished = false;

    private float timer;
    private bool isFinished;
    private GameObject coinCollector; //Coin manager que almacena las monedas que vas consiguiendo

    private void Awake()
    {
        GameManager.Instance.StartMinigame();
    }

    private void Start()
    {
        tutPanel.OpenPanel();
        isFinished = true;
    }

    public void StartGame()
    {
        isFinished = false;
        timer = timeToPlay;
        coinCollector = GameObject.Find("CoinCollector");
        timeSlider.maxValue = timer;
    }

    private void Update()
    {
        if (!isFinished)
        {
            //timer disminuyendo, cuando sea 0 sale el panel con la score
            timer -= Time.deltaTime;
            timeSlider.value = timer;
            if (timer <= 0)
            {
                isFinished = true;
                timer = timeToPlay;
                FinishGame();
            }
        }       
    }

    public void FinishGame()
    {
        coinsText.text = "SCORE \n" + coinCollector.GetComponent<CoinCollector>().CoinsCollected.ToString();
        GameFinished = true;
        finishPanel.SetActive(true);

        GameManager.Instance.FinishFunMinigame(coinCollector.GetComponent<CoinCollector>().CoinsCollected);
        GameManager.Instance.MoneyToAdd += coinCollector.GetComponent<CoinCollector>().CoinsCollected;
    }
}
