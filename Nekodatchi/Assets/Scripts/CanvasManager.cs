﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CanvasManager : Singleton<CanvasManager>
{
    public GameObject MainPanel;
    public GameObject ShopPanel;
    public GameObject DiePanel;
    public float TimeToFadeDiePanel;

    public Button StoreButton;

    public Image HungerImage;
    public Image HygieneImage;
    public Image FunImage;
    public Image EnergyImage;


    public Transform CoinImage;

    public void ShowMainPanel()
    {
        ShopPanel.SetActive(false);
        MainPanel.SetActive(true);
    }
    public void ShowShopPanel()
    {
        MainPanel.SetActive(false);
        ShopPanel.SetActive(true);
    }

    public void ShowDiePanel()
    {
        DiePanel.GetComponent<Image>().raycastTarget = true;
        DiePanel.GetComponent<CanvasGroup>().blocksRaycasts = true;

        if (DiePanel.GetComponent<CanvasGroup>() != null) {
            DiePanel.GetComponent<CanvasGroup>().DOFade(1, TimeToFadeDiePanel).SetEase(Ease.Linear).Play().onComplete = () => { GameManager.Instance.GetNewCat();
                Invoke("FadeOutDiePanel", 1);
            };

        }
    }

    public void FadeOutDiePanel()
    {
        DiePanel.GetComponent<CanvasGroup>().DOFade(0, TimeToFadeDiePanel).SetEase(Ease.Linear).Play().onComplete = () =>
        {
            DiePanel.GetComponent<Image>().raycastTarget = false;
            DiePanel.GetComponent<CanvasGroup>().blocksRaycasts = false;
        };
    }

    public void UpdateResourcesImage()
    {
        HungerImage.fillAmount = GameManager.Instance.Hunger / GameManager.Instance.MaxResourceValue;
        HygieneImage.fillAmount = GameManager.Instance.Hygiene / GameManager.Instance.MaxResourceValue;
        FunImage.fillAmount = GameManager.Instance.Fun / GameManager.Instance.MaxResourceValue;
        EnergyImage.fillAmount = GameManager.Instance.Energy / GameManager.Instance.MaxResourceValue;
    }

    public Transform GetCoinHUDPos()
    {
        return CoinImage;
    }

    public void DisableStore()
    {
        StoreButton.enabled = false;
        StoreButton.interactable = false;
    }

    public void EnableStore()
    {
        StoreButton.enabled = true;
        StoreButton.interactable = true;
    }
}
