﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorPos : MonoBehaviour
{

    public Transform LimitLeft, LimitRight;

    // Update is called once per frame
    void Update()
    {
        Vector3 ScreenPoint = Input.mousePosition;
        ScreenPoint.z = Camera.main.transform.position.z;
        if(LimitLeft.position.x < Camera.main.ScreenToWorldPoint(ScreenPoint).x && LimitRight.position.x > Camera.main.ScreenToWorldPoint(ScreenPoint).x)
            transform.position = new Vector3(Camera.main.ScreenToWorldPoint(ScreenPoint).x, transform.position.y, transform.position.z);

    }

    
}
