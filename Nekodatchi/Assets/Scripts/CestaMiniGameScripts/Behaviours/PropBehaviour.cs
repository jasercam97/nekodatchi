﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropBehaviour : MonoBehaviour
{

    public PropType type;

    public Sprite sprite;

    public int AddScore;

    private float _speed;

    private void OnEnable()
    {
        GetComponent<SpriteRenderer>().sprite = sprite;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.down * _speed * Time.deltaTime);
    }

    public void setSpeed(Vector2 SpeedRange)
    {
        _speed = Random.Range(SpeedRange.x, SpeedRange.y);
    }

   
}
