﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatMovement : MonoBehaviour
{

    public bool MoveTowardsCursor;
    public float Speed;

    public Transform CursorPosition;

    public ParticleSystem success;
    public ParticleSystem fail;
    public bool finished;

    public AudioClip SuccessClip;
    public AudioClip FailClip;


    private bool _inPosition;
    private CestaGameManager manager;

    void Start()
    {
        manager = GameObject.FindObjectOfType<CestaGameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        

        if (!MoveTowardsCursor)
            transform.position = new Vector3(CursorPosition.position.x, transform.position.y, transform.position.z);
        else
        {
            if (!_inPosition)
            {

                if ((CursorPosition.position - transform.position).x < 0)
                    transform.Translate(Vector2.left * Speed * Time.deltaTime);
                else if ((CursorPosition.position - transform.position).x > 0)
                    transform.Translate(Vector2.right * Speed * Time.deltaTime);
            }
        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pointer" && !finished)
            _inPosition = true;
        else if (collision.tag == "Prop" && !finished)
        {

            if (collision.GetComponent<PropBehaviour>().type == PropType.Coin)
            {
                GameManager.Instance.MoneyToAdd += 1;
                AudioManager.Instance.PlayOneClipSFX(SuccessClip);
                success.Play();
            }
            else
            {
                manager.AddScore(collision.GetComponent<PropBehaviour>().AddScore);
                if (collision.GetComponent<PropBehaviour>().AddScore > 0)
                {
                    success.Play();
                    AudioManager.Instance.PlayOneClipSFX(SuccessClip);
                }
                else
                {
                    fail.Play();
                    AudioManager.Instance.PlayOneClipSFX(FailClip);
                }
            }

            collision.gameObject.SetActive(false);

        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Pointer")
            _inPosition = false;
    }

    
}
