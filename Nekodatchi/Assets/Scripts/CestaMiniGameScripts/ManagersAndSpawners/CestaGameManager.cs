﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CestaGameManager : MonoBehaviour
{

    public int coins;

    public int score;

    public int TimeForGame;

    [Header("Cosas que activar para empezar")]
    public CatMovement catMovementScript;
    public CestaPropSpawner cestaPropSpawnerScript;


    [Header("HUD")]
    public TextMeshProUGUI scoreText;
    public Slider TimeSlider;
    public GameObject EndPanel;
    public TextMeshProUGUI finalScore;
    public TutorialPanel tutPanel;
    public FadePanel fade;
    

    private int _timer;
    private Coroutine _timerCoroutine;


    void Start()
    {
        GameManager.Instance.StartMinigame();

        scoreText.text = "Score: \n x 0";
        TimeSlider.maxValue = TimeForGame;
        fade.FadeOut().onComplete = () => { tutPanel.OpenPanel(); };
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        catMovementScript.enabled = true;
        cestaPropSpawnerScript.enabled = true;
        cestaPropSpawnerScript.GamePlaying = true;
        _timerCoroutine = StartCoroutine(Timer());
    }

    public void AddScore(int i)
    {
        if(_timer > 0)
        {
            if (score + i >= 0)
                score += i;
            else
                score = 0;

            scoreText.text = "Score: \nx " + score.ToString();
        }

    }

    IEnumerator Timer()
    {
        _timer = TimeForGame;

        while (_timer > 0)
        {
            TimeSlider.value = _timer;
            yield return new WaitForSeconds(1f);
            _timer -= 1;
            TimeSlider.value = _timer;
        }

        FinishGame();
    }

    private void FinishGame()
    {
        cestaPropSpawnerScript.GamePlaying = false;
        catMovementScript.finished = true;
        catMovementScript.enabled = false;

        finalScore.text = "Final Score:\n" + score;

        EndPanel.SetActive(true);

        GameManager.Instance.FinishHungerGame(score);
    }
}
