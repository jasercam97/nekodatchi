﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CestaPropSpawner : MonoBehaviour
{
    [Header("Rangos")]
    public Vector2 SpawnTimeRange;

    public Vector2 SpeedRange;

    [Header("Configuracion de props")]

    public Transform spawnPositionLeft;
    public Transform spawnPositionRight;

    public Transform PropPool;

    public GameObject PropPrefab;

    public List<Sprite> FishSprite;

    public Sprite PickleSprite;

    public Sprite CoinSprite;

    [Header("Cuanto Suma cada Prop")]
    public int AddScore;
    public int AddCoins;
    [Range(-10, -1)]
    public int SubstractScore;

    [Header("Juego en curso")]
    public bool GamePlaying;

    

    void Start()
    {

        GamePlaying = true;

        StartCoroutine(Spawn());
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator Spawn()
    {

        while(GamePlaying)
        {

            GameObject aux = PoolManager.Instance.GetFreeObject(PropPool, PropPrefab);

            aux.transform.position = new Vector3(
                Random.Range(spawnPositionLeft.position.x, spawnPositionRight.position.x), 
                spawnPositionLeft.position.y, 
                spawnPositionLeft.position.z
                );

            aux.GetComponent<PropBehaviour>().setSpeed(SpeedRange);

            int type = Random.Range(0,3);

            switch(type)
            {
                case 0: 
                    aux.GetComponent<PropBehaviour>().sprite = FishSprite[Random.Range(0, FishSprite.Count)];
                    aux.GetComponent<PropBehaviour>().type = PropType.Fish;
                    aux.GetComponent<PropBehaviour>().AddScore = AddScore;
                    break;
                case 1:
                    aux.GetComponent<PropBehaviour>().sprite = PickleSprite;
                    aux.GetComponent<PropBehaviour>().type = PropType.Pickle;
                    aux.GetComponent<PropBehaviour>().AddScore = SubstractScore;
                    break;
                case 2:
                    aux.GetComponent<PropBehaviour>().sprite = CoinSprite;
                    aux.GetComponent<PropBehaviour>().type = PropType.Coin;
                    aux.GetComponent<PropBehaviour>().AddScore = AddCoins;
                    break;
            }

            aux.SetActive(true);

            yield return new WaitForSeconds(Random.Range(SpawnTimeRange.x, SpawnTimeRange.y));

        }
    }
    
}
