﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : Singleton<PoolManager>
{
    public GameObject GetFreeObject(Transform poolParent, GameObject objectPrefab)
    {
        GameObject freeObject = null;

        foreach(Transform child in poolParent)
        {
            if (!child.gameObject.activeInHierarchy)
            {
                freeObject = child.gameObject;
                break;
            }
        }
 

        if (freeObject == null)
        {
            freeObject = Instantiate(objectPrefab, poolParent);
        }

        return freeObject;
    }
}