﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    //public Texture2D CursorTexture;

    [Header("RESOURCES GENERAL")]
    public float MaxResourceValue;
    public float StartingValue;
    public float PoopValue;

    [Space(10)]
    public float ResourceLossTime;
    public int MinResourceLoss;
    public int MaxResourceLoss;

    [Header("SLEEP")]
    public float EnergyResourceGain;
    public float SleepMinResourceLoss;
    public float SleepMaxResourceLoss;

    [Header("MINIGAMES")]
    public float MinigameResourceGain;
    public float HungerTargetPoints;
    public float HygieneTargetPoints;
    public float FunTargetPoints;

    [Header("Sounds")]
    public AudioClip Win;
    public AudioClip Lose;

    private bool _onMinigame = false;
    private bool _gameStarted = false;

    public float Hunger { 
        get { return _hunger; }
        set { _hunger = CheckUpdatedValue(value); } 
    }

    public float Hygiene {
        get { return _hygiene; } 
        set { _hygiene = CheckUpdatedValue(value); } 
    }

    public float Energy { 
        get { return _energy; } 
        set { _energy = CheckUpdatedValue(value); } 
    }

    public float Fun {
        get { return _fun; } 
        set { _fun = CheckUpdatedValue(value); }
    }

    public float Money
    {
        get { return _money; }
        set { _money = UpdateMoney(value); }
    }

    [Header("Money FX")]
    public int MoneyToAdd;

    public float TimesPet { get; set; }

    private float _hunger;
    private float _hygiene;
    private float _energy;
    private float _fun;
    private float _money;
    private float _pets;

    private void Awake()
    {
        Money = PlayerPrefs.GetFloat("Money", 0);

        DontDestroyOnLoad(gameObject);

        //Cursor.visible = false;   // TODO poner
        //Cursor.SetCursor(CursorTexture, new Vector2(4, 4), CursorMode.ForceSoftware);

        InvokeRepeating(nameof(LoseResources), ResourceLossTime, ResourceLossTime);
    }

    public void GetNewCat()
    {
        Hunger = StartingValue;
        Hygiene = StartingValue;
        Energy = StartingValue;
        Fun = StartingValue;
        TimesPet = 0;

        UpdateResourceImages();

        EvolutionManager.Instance.InitializeEvolution();
    }

    private float CheckUpdatedValue(float value)
    {
        if (value > MaxResourceValue)
        {
            return MaxResourceValue;
        }
        else if (value < 0)
        {
            return 0;
        }
        else
        {
            return value;
        }
    }

    private float UpdateMoney(float value)
    {
        if (_gameStarted)
        {
            ShopManager.Instance.UpdateMoney();
        }

        PlayerPrefs.SetFloat("Money", value);
        return value;
    }

    private void LoseResources()
    {
        Hunger -= Random.Range(MinResourceLoss, MaxResourceLoss);
        Hygiene -= Random.Range(MinResourceLoss, MaxResourceLoss);
        Energy -= Random.Range(MinResourceLoss, MaxResourceLoss);
        Fun -= Random.Range(MinResourceLoss, MaxResourceLoss);

        if (!_onMinigame)
        {
            UpdateResourceImages();

            if (Hunger <= 0 || Hygiene <= 0 || Energy <= 0 || Fun <= 0)
            {
                Die();
            }
        }
    }

    private void Die()
    {
        CanvasManager.Instance.ShowDiePanel();
    }

    public void Sleep()
    {
        Energy += EnergyResourceGain;

        Hunger -= Random.Range(SleepMinResourceLoss, SleepMaxResourceLoss);
        Hygiene -= Random.Range(SleepMinResourceLoss, SleepMaxResourceLoss);
        Fun -= Random.Range(SleepMinResourceLoss, SleepMaxResourceLoss);

        UpdateResourceImages();
        TryEvolve();
    }

    public void StartMinigame()
    {
        _onMinigame = true;
        MoneyToAdd = 0;
    }

    public void FinishHungerGame(float points)
    {
        EndSoundSFX(points, HungerTargetPoints);
        Hunger += GetResourceGain(points, HungerTargetPoints);
    }


    public void FinishHygieneGame(float points)
    {
        EndSoundSFX(points, HygieneTargetPoints);
        Hygiene += GetResourceGain(points, HygieneTargetPoints);
    }

    public void FinishFunMinigame(float points)
    {
        EndSoundSFX(points, FunTargetPoints);
        Fun += GetResourceGain(points, FunTargetPoints);
    }
    private void EndSoundSFX(float points, float Target)
    {
        if (points > Target)
            AudioManager.Instance.PlayOneClipSFX(Win);
        else
            AudioManager.Instance.PlayOneClipSFX(Lose);
    }

    public void CompleteMinigame()
    {
        if (!_gameStarted)  // Para la primera vez que carga el juego
        {
            _gameStarted = true;
            GetNewCat();
        }

        CoinVFX.Instance.AddCoins(Vector3.zero, MoneyToAdd);
        _onMinigame = false;
        UpdateResourceImages();
        TryEvolve();
    }

    private float GetResourceGain(float earnedPoints, float targetPoints)
    {
        return earnedPoints >= targetPoints ? MinigameResourceGain : earnedPoints / targetPoints * MinigameResourceGain;
    }

    private void UpdateResourceImages()
    {
        CanvasManager.Instance.UpdateResourcesImage();
    }

    private void TryEvolve()
    {
        EvolutionManager.Instance.TryEvolve(Hunger, Hygiene, Fun, Energy, TimesPet);
    }

    public void CleanPoop()
    {
        Hygiene += PoopValue;
        UpdateResourceImages();
    }
}