﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetUpSoundOptions : MonoBehaviour
{
    public Slider GeneralSlider;
    public Slider MusicSlider;
    public Slider SFXSlider;

    public AudioMixer Mixer;

    private float _sliderValue;

    void Start()
    {
        SetSliders();
    }

    private void SetSliders()
    {
        Mixer.GetFloat(AudioManager.GENERAL_VOLUME_TAG, out _sliderValue);
        GeneralSlider.value = LogConversion(_sliderValue);

        Mixer.GetFloat(AudioManager.MUSIC_VOLUME_TAG, out _sliderValue);
        MusicSlider.value = LogConversion(_sliderValue);

        Mixer.GetFloat(AudioManager.SFX_VOLUME_TAG, out _sliderValue);
        SFXSlider.value = LogConversion(_sliderValue);
    }

    private float LogConversion(float value)
    {
        return Mathf.Pow(10, value / 20);
    }
}
