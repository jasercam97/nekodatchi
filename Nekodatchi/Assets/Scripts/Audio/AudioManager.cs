﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    public const float DefaultVolumeSFX = 0.55f;
    public const float DefaultVolumeMusic = 0.35f;

    public static string GENERAL_VOLUME_TAG = "GeneralVolume";
    public static string MUSIC_VOLUME_TAG = "MusicVolume";
    public static string SFX_VOLUME_TAG = "EffectVolume";


    public AudioSource SFX_Source;
    public AudioSource Music_Source;

    public AudioMixer Mixer;

    public AudioClip ButtonClickSound;
    public AudioClip ButtonHoverSound;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    #region Audio Play Methods
    public void PlayOneClipSFX(AudioClip clip, float volume = DefaultVolumeSFX)
    {
        SFX_Source.PlayOneShot(clip, volume);
    }

    public void PlayMusic(AudioClip clip, float volume = DefaultVolumeMusic)
    {
        Music_Source.clip = clip;
        Music_Source.volume = volume;
        Music_Source.Play();
    }

    public void PlayLoopSFX(AudioClip clip, float volume = DefaultVolumeSFX)
    {
        SFX_Source.loop = true;
        SFX_Source.clip = clip;
        SFX_Source.volume = volume;
        SFX_Source.Play();
    }

    public void StopLoopSFX(AudioClip clip, float volume = DefaultVolumeSFX)
    {
        SFX_Source.loop = false;
        SFX_Source.Stop();
    }

    public void StopMusic()
    {
        Music_Source.Stop();
    }

    public void PlayButtonHoverSound()
    {
        PlayOneClipSFX(ButtonHoverSound);
    }

    public void PlayButtonClickSound()
    {
        PlayOneClipSFX(ButtonClickSound);
    }
    #endregion

    #region Audio Mixer
    public void setGeneralVolume(float v)
    {
        v = Mathf.Clamp(v, 0.001f, Mathf.Infinity);
        Mixer.SetFloat(GENERAL_VOLUME_TAG, Mathf.Log10(v) * 20);
    }

    public void setMusicVolume(float v)
    {
        v = Mathf.Clamp(v, 0.001f, Mathf.Infinity);
        Mixer.SetFloat(MUSIC_VOLUME_TAG, Mathf.Log10(v) * 20);
    }

    public void setEffectVolume(float v)
    {
        v = Mathf.Clamp(v, 0.001f, Mathf.Infinity);
        Mixer.SetFloat(SFX_VOLUME_TAG, Mathf.Log10(v) * 20);
    }
    #endregion

}
