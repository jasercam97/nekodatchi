﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameEquipment : MonoBehaviour
{
    public CatEquipment Equipment;
    public List<CatEvolution> EvolutionList;

    private void Start()
    {
        FindEvolution();
        FindEquipment();
    }

    private void FindEvolution()
    {
        foreach (Evolutions evolution in Enum.GetValues(typeof(Evolutions)))
        {
            if (PlayerPrefs.GetInt(evolution.ToString(), 0) == 1)   // 1  es esa evolucion
            {
                EvolutionList.Find(e => e.Evolution == evolution).CatObject?.SetActive(true);
            }
        }
    }

    private void FindEquipment()
    {
        GameObject currentObject;

        foreach (ShopItems item in Enum.GetValues(typeof(ShopItems)))
        {
            if (PlayerPrefs.GetInt(item.ToString(), 0) == 2)    // 2 = equipado
            {
                currentObject = Equipment.EquipmentList.Find(e => e.ItemID == item).ItemObject;

                if (currentObject != null)
                {
                    currentObject.SetActive(true);
                }
            }
        }
    }
}
