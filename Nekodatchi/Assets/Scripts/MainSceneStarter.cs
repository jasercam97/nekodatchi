﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSceneStarter : MonoBehaviour
{
    void Start()
    {
        FinishPreviousMinigame();
    }

    private void FinishPreviousMinigame()
    {
        GameManager.Instance.CompleteMinigame();
    }
}
