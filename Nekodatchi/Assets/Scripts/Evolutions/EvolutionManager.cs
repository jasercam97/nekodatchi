﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvolutionManager : Singleton<EvolutionManager>
{
    public Evolutions CurrentEvolution { get; set; }

    public List<CatEvolution> EvolutionList;

    private void Awake()
    {
        foreach (CatEvolution evolution in EvolutionList)
        {
            if (PlayerPrefs.GetInt(evolution.Evolution.ToString(), 0) == 1)
            {
                CurrentEvolution = evolution.Evolution;
                ActivateCat(CurrentEvolution);
            }
        }
    }

    public void InitializeEvolution()
    {
        CurrentEvolution = Evolutions.None;
        CanvasManager.Instance.ShowMainPanel();
        CanvasManager.Instance.DisableStore();
        ShopManager.Instance.UnequipItems();

        ActivateCat(CurrentEvolution);
    }

    public void TryEvolve(float hunger, float hygyiene, float fun, float energy, float pets)
    {
        if (CurrentEvolution == Evolutions.None)
        {
            CurrentEvolution = EvolutionDatabase.Instance.CheckEvolution(hunger, hygyiene, fun, energy, pets);
        }

        ActivateCat(CurrentEvolution);
    }

    private void ActivateCat(Evolutions targetEvolution)
    {
        if (targetEvolution != Evolutions.None)
        {
            CanvasManager.Instance.EnableStore();
        }

        foreach (CatEvolution evolution in EvolutionList)
        {
            if (evolution.Evolution == targetEvolution)
            {
                evolution.CatObject.SetActive(true);
                PlayerPrefs.SetInt(evolution.Evolution.ToString(), 1);
            }
            else
            {
                evolution.CatObject.SetActive(false);
                PlayerPrefs.SetInt(evolution.Evolution.ToString(), 0);
            }
        }
    }
}

[System.Serializable]
public class CatEvolution
{
    public Evolutions Evolution;
    public GameObject CatObject;
}