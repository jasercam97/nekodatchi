﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvolutionDatabase : Singleton<EvolutionDatabase>
{
    public List<EvolutionData> EvolutionDataList;
    public float LowResources;

    public Evolutions CheckEvolution(float hunger, float hygiene, float fun, float energy, float pets)
    {
        foreach(EvolutionData evolutionData in EvolutionDataList)
        {
            if (hunger <= LowResources && hygiene <= LowResources && fun <= LowResources)
            {
                if (pets == 0)
                {
                    return Evolutions.Normal;
                }
                else
                {
                    return Evolutions.Blanco;
                }
            }
            else if (hunger >= evolutionData.TargetHunger && hygiene >= evolutionData.TargetHygiene && fun >= evolutionData.TargetFun
                    && energy >= evolutionData.TargetEnergy && pets >= evolutionData.TargetPets)
            {
                return evolutionData.Evolution;
            }
        }

        return Evolutions.None;
    }
}

[System.Serializable]
public class EvolutionData
{
    public Evolutions Evolution;
    public float TargetHunger;
    public float TargetHygiene;
    public float TargetFun;
    public float TargetEnergy;
    public float TargetPets;
}