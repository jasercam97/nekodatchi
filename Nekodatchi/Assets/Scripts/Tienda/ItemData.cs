﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "ShopItem", menuName = "ScriptableObjects/ShopItem")]
public class ItemData : ScriptableObject
{
    public ShopItems ItemID;
    public ItemType Type;
    public string DisplayName;
    public Sprite Image;
    public float Cost;
    [TextArea]
    public string Description;
}
