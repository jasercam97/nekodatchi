﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatEquipment : MonoBehaviour
{
    public List<EquipData> EquipmentList;

    private GameObject _currentItem;

    public void EquipItem(ShopItems item)
    {
        _currentItem = EquipmentList.Find(i => i.ItemID == item).ItemObject;

        _currentItem.SetActive(true);
    }

    public void UnequipItem(ShopItems item)
    {
        _currentItem = EquipmentList.Find(i => i.ItemID == item).ItemObject;

        _currentItem.SetActive(false);
    }
}

[System.Serializable]
public class EquipData
{
    public ShopItems ItemID;
    public GameObject ItemObject;
}
