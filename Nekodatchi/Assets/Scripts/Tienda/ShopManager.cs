﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class ShopManager : Singleton<ShopManager>
{
    public CatEquipment Equipment;
    public List<ItemData> ItemList;

    [Header("Money")]
    public TMP_Text MoneyText;

    [Header("Buttons panel")]
    public Transform SkinItemsParent;
    public Transform TrailItemsParent;
    public Transform BackgroundItemsParent;
    public GameObject ItemButtonPrefab;

    [Header("Description panel")]
    public TMP_Text ItemName;
    public TMP_Text ItemCost;
    public Image ItemImage;
    public TMP_Text ItemDescription;
    public TMP_Text BuyButtonText;

    private ItemData _selectedItem;
    private List<ItemData> _boughtItems;
    private List<ItemData> _equippedItems;

    private List<ItemButton> _buttonList;

    private void Awake()
    {
        _boughtItems = new List<ItemData>();
        _equippedItems = new List<ItemData>();
        _buttonList = new List<ItemButton>();

        if (PlayerPrefs.GetInt(ShopItems.BackgroundGreen.ToString(), 2) != 2)
        {
            PlayerPrefs.SetInt(ShopItems.BackgroundGreen.ToString(), 1);
        }
        else
        {
            PlayerPrefs.SetInt(ShopItems.BackgroundGreen.ToString(), 2);
        }
    }

    private void Start()
    {
        InitializeShop();
    }

    private void InitializeShop()
    {
        LoadData();
        SortItems();
        FillButtonList();
        UpdateMoney();
    }

    private void LoadData()
    {
        ItemData _currentItem;

        foreach (ShopItems item in Enum.GetValues(typeof(ShopItems)))
        {
            _currentItem = ItemList.Find(i => i.ItemID == item);

            if (PlayerPrefs.GetInt(item.ToString(), 0) > 0)
            {
                _boughtItems.Add(_currentItem);
            }

            if (PlayerPrefs.GetInt(item.ToString(), 0) == 2)
            {
                _equippedItems.Add(_currentItem);
                EquipItem(_currentItem);
            }
        }
    }

    private void SortItems()
    {
        ItemList.Sort((x, y) => x.Cost.CompareTo(y.Cost));
    }

    private void FillButtonList()
    {
        ItemButton currentButton;

        foreach (ItemData item in ItemList)
        {
            switch (item.Type)
            {
                case ItemType.Skin:
                    currentButton = Instantiate(ItemButtonPrefab, SkinItemsParent).GetComponent<ItemButton>();
                    break;

                case ItemType.Trail:
                    currentButton = Instantiate(ItemButtonPrefab, TrailItemsParent).GetComponent<ItemButton>();
                    break;

                case ItemType.Background:
                    currentButton = Instantiate(ItemButtonPrefab, BackgroundItemsParent).GetComponent<ItemButton>();
                    break;

                default:
                    currentButton = Instantiate(ItemButtonPrefab, SkinItemsParent).GetComponent<ItemButton>();
                    break;
            }

            currentButton.InitializeButton(item);
            _buttonList.Add(currentButton);

            if (_equippedItems.Contains(item))
            {
                currentButton.SetEquipped();
            }
            else if (_boughtItems.Contains(item))
            {
                currentButton.SetUnequipped();
            }
        }

        SelectItem(ItemList[0]);
    }

    public void CheckItem()
    {
        if (_equippedItems.Contains(_selectedItem))
        {
            if (_selectedItem.Type != ItemType.Background)
            {
                UnequipItem(_selectedItem);
            }
        }
        else if (_boughtItems.Contains(_selectedItem))
        {
            EquipItem(_selectedItem);
        }
        else
        {
            BuyItem(_selectedItem);
        }
    }

    private void EquipItem(ItemData itemData)
    {
        ItemData equippedItem = _equippedItems.Find(i => i.Type == itemData.Type);

        if (equippedItem != null)
        {
            UnequipItem(equippedItem);
        }

        _equippedItems.Add(itemData);

        if (itemData.Type == ItemType.Background)
        {
            BuyButtonText.text = "Equipped";
        }
        else
        {
            BuyButtonText.text = "Unequip";
        }

        UpdateButtons();

        Equipment.EquipItem(itemData.ItemID);
        
        PlayerPrefs.SetInt(itemData.ItemID.ToString(), 2);     // 2 == Equipado
    }

    private void UnequipItem(ItemData itemData)
    {
        _equippedItems.Remove(itemData);
        BuyButtonText.text = "Equip";
        UpdateButtons();

        Equipment.UnequipItem(itemData.ItemID);
        
        PlayerPrefs.SetInt(itemData.ItemID.ToString(), 1);     // 1 == Comprado
    }

    private void BuyItem(ItemData itemData)
    {
        if (itemData.Cost <= GameManager.Instance.Money)
        {
            GameManager.Instance.Money -= itemData.Cost;
            _boughtItems.Add(itemData);
            EquipItem(itemData);    // Se equipa automáticamente al comprarlo
            UpdateMoney();
        }
    }

    public void UpdateButtons()
    {
        foreach(ItemButton button in _buttonList)
        {
            if (_equippedItems.Contains(button.ItemData))
            {
                button.SetEquipped();
            }
            else if (_boughtItems.Contains(button.ItemData))
            {
                button.SetUnequipped();
            }
        }
    }

    public void SelectItem(ItemData itemData)
    {
        _selectedItem = itemData;
        UpdateInfo();
    }

    public void UpdateInfo()
    {
        ItemName.text = _selectedItem.DisplayName;
        ItemImage.sprite = _selectedItem.Image;
        ItemDescription.text = _selectedItem.Description;

        if (_equippedItems.Contains(_selectedItem))
        {
            if (_selectedItem.Type == ItemType.Background)
            {
                ItemCost.text = "Equipped";
            }
            else
            {
                ItemCost.text = "Unequip";
            }
        }
        else if (_boughtItems.Contains(_selectedItem))
        {
            ItemCost.text = "Equip";
        }
        else
        {
            ItemCost.text = _selectedItem.Cost.ToString();
        }
    }

    public void ShowSkinsPanel()
    {
        SkinItemsParent.gameObject.SetActive(true);
        TrailItemsParent.gameObject.SetActive(false);
        BackgroundItemsParent.gameObject.SetActive(false);
    }

    public void ShowTrailsPanel()
    {
        SkinItemsParent.gameObject.SetActive(false);
        TrailItemsParent.gameObject.SetActive(true);
        BackgroundItemsParent.gameObject.SetActive(false);
    }

    public void ShowBackgroundsPanel()
    {
        SkinItemsParent.gameObject.SetActive(false);
        TrailItemsParent.gameObject.SetActive(false);
        BackgroundItemsParent.gameObject.SetActive(true);
    }

    public void UpdateMoney()
    {
        MoneyText.text = "x" + GameManager.Instance.Money;
    }

    public void UnequipItems()
    {
        List<ItemData> itemsToUnequip = new List<ItemData>();

        foreach (ItemData item in _equippedItems)
        {
            if (item.Type == ItemType.Skin || item.Type == ItemType.Trail)
            {
                itemsToUnequip.Add(item);
            }
        }

        foreach (ItemData item in itemsToUnequip)
        {
            UnequipItem(item);
        }
    }
}