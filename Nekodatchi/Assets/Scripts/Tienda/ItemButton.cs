﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    public Image ItemImage;
    public ItemData ItemData;
    public TMP_Text StatusText;

    public void InitializeButton(ItemData itemData)
    {
        ItemData = itemData;

        ItemImage.sprite = itemData.Image;

        StatusText.text = "";
    }

    public void SelectItem()
    {
        ShopManager.Instance.SelectItem(ItemData);
    }

    public void SetEquipped()
    {
        StatusText.text = "Equipped";
    }

    public void SetUnequipped()
    {
        StatusText.text = "Bought";
    }
}
