﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadePanel : MonoBehaviour
{

    public float FadeDuration;

    private Image _image;

    private void Awake()
    {
        _image = GetComponent<Image>();
        Color temp = _image.color;
        temp.a = 1;
        _image.color = temp;
    }

    private void Start()
    {
        FadeOut();
    }

    public Sequence FadeOut()
    {
        Sequence seq = DOTween.Sequence();

        seq.Append(_image.DOFade(0, FadeDuration).SetEase(Ease.Linear));

        return seq.Play();
    }

    public Sequence FadeIn()
    {

        Sequence seq = DOTween.Sequence();

        seq.Append(_image.DOFade(1, FadeDuration).SetEase(Ease.Linear));

        return seq.Play();

    }

    public void SleepFade()
    {
        Sequence seq = DOTween.Sequence();

        _image.raycastTarget = true;

        seq.Append(_image.DOFade(1, FadeDuration).SetEase(Ease.Linear).OnComplete(() => GameManager.Instance.Sleep()));
        seq.Append(_image.DOFade(0, FadeDuration).SetDelay(.5f).SetEase(Ease.Linear).OnComplete(() => _image.raycastTarget = false));
        seq.Play();
    }

    public void FadeIn(int index)
    {
        _image.raycastTarget = true;
        _image.DOFade(1, FadeDuration).SetEase(Ease.Linear).Play().onComplete = () => { SceneManager.LoadScene(index); };
    }

    public void FadeInWithoutSceneChange()
    {
        _image.DOFade(1, FadeDuration).SetEase(Ease.Linear).Play();
    }

    public void FadeOutWithoutSceneChange()
    {
        _image.DOFade(0, FadeDuration).SetEase(Ease.Linear).Play();
    }
}
