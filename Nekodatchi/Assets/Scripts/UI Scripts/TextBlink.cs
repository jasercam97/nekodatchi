﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextBlink : MonoBehaviour
{
    public float BlinkTime;

    private TMP_Text _text;
    private float _timer;

    private void Awake()
    {
        _text = GetComponent<TMP_Text>();
    }

    private void Update()
    {
        _timer += Time.deltaTime;

        if(_timer >= BlinkTime)
        {
            _text.enabled = !_text.enabled;
            _timer = 0;
        }
    }
}
