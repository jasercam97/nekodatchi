﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PanelAppear : MonoBehaviour
{
    public float Duration;

    private void OnEnable()
    {
        transform.localScale = Vector2.zero;
        transform.DOScale(1, Duration).SetEase(Ease.Linear).Play();    
    }
}
