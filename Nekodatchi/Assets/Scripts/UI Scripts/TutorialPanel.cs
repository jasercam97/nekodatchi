﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TutorialPanel : MonoBehaviour
{

    public void OpenPanel()
    {
        transform.DOScale(1, 0.3f).SetEase(Ease.InOutQuad).Play();
    }

    public void ClosePanel()
    {
        transform.DOScale(0.0001f, 0.3f).SetEase(Ease.InQuad).Play().onComplete = () => { gameObject.SetActive(false); };
        
    }

    
}
